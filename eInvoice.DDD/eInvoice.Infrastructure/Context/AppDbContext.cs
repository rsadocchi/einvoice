﻿using eInvoice.Domain.AggregateModels;
using eInvoice.Domain.SeedWork;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace eInvoice.Infrastructure
{
    public class AppDbContext : DbContext, IUnitOfWork
    {

        public AppDbContext(DbContextOptions options) : base(options: options) { }

        public async Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default(CancellationToken)) 
            => (await base.SaveChangesAsync() > 0 ? true : false);

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

    }
}
