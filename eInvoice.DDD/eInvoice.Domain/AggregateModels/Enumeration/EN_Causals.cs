﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    public class EN_Causals : Enumeration<int>
    {
        [Key] public int C_ID { get; set; }
        [Required] [StringLength(5)] public string C_Code { get; set; }
        [StringLength(50)] public string C_Description { get; set; }

        public EN_Causals() { }
        public EN_Causals(int id, string code, string description) : base(id, code)
        {
            C_ID = id;
            C_Code = code;
            C_Description = description;
        }

        public static IEnumerable<EN_Causals> GetAll() => GetAll<EN_Causals>();

        public static EN_Causals Payment = new EN_Causals(10, "PAY", "Payment");
        public static EN_Causals Invoice = new EN_Causals(20, "INV", "Invoice");

    }
}
