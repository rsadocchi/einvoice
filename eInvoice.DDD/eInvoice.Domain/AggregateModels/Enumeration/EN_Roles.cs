﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "EN_Roles")]
    public class EN_Roles : Enumeration<int>
    {
        [Key] public int R_ID { get; set; }
        [Required] [StringLength(5)] public string R_Code { get; set; }
        [StringLength(50)] public string R_Description { get; set; }

        //public virtual ICollection<IAggregateRoot> EntityRef { get; set; }

        public EN_Roles() { }

        public EN_Roles(int id, string code, string description = null/*, ICollection<IAggregateRoot> entityRef = null*/) : base(id, code)
        {
            R_ID = id;
            R_Code = code;
            R_Description = description;
            //EntityRef = entityRef;
        }

        public static IEnumerable<EN_Roles> GetAll() => GetAll<EN_Roles>();

        public static EN_Roles Customer = new EN_Roles(10, "CST", "Customer");
        public static EN_Roles Supplier = new EN_Roles(20, "SPL", "Supplier");
    }
}
