﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "EN_DocTypes", Schema = "dbo")]
    public class EN_DocTypes : Enumeration<int>
    {
        [Key] public int DT_ID { get; set; }
        [Required] [StringLength(5)] public string DT_Code { get; set; }
        [StringLength(50)] public string DT_Description { get; set; }

        public EN_DocTypes() { }

        public EN_DocTypes(int id, string code, string description = null) : base(id, code)
        {
            DT_ID = id;
            DT_Code = code;
            DT_Description = description;
        }

        public static IEnumerable<EN_DocTypes> GetAll() => GetAll<EN_DocTypes>();

        public static EN_DocTypes DDT = new EN_DocTypes(10, "DDT", "Transport document");
        public static EN_DocTypes Invoice = new EN_DocTypes(20, "INV", "Invoice");
    }
}
