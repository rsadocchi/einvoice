﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    public class Fin_Master : Entity, IAggregateRoot
    {


        public virtual ICollection<Fin_Master> Acc_MasterDDTs { get; set; }
        public virtual ICollection<Fin_Master> Acc_MasterInvoices { get; set; }

        public Fin_Master()
        {
            Acc_MasterDDTs = new HashSet<Fin_Master>();
            Acc_MasterInvoices = new HashSet<Fin_Master>();
        }
    }
}
