﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "Fin_DocXMov", Schema = "dbo")]
    public class Fin_DocXMov : Entity, IAggregateRoot
    {
        [Key] public int FCXM_ID { get; set; }
        [Required] public int FCXM_FinMasterID { get; set; }
        [Required] public int FCXM_DocMasterID { get; set; }
        public bool? FCXM_Partial { get; set; }
    }
}
