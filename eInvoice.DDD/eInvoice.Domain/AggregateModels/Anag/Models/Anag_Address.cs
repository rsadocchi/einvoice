﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "Anag_Address")]
    public class Anag_Address : Entity, IAggregateRoot
    {
        [Key] public int AA_ID { get; set; }
        public int? AA_AnagMasterID { get; set; }
        public int? AA_ContactID { get; set; }
        [Required] [StringLength(5)] public string AA_Country { get; set; }
        [Required] [StringLength(5)] public string AA_CountrySpec { get; set; }
        [Required] [StringLength(10)] public string AA_PostCode { get; set; }
        [Required] [StringLength(50)] public string AA_City { get; set; }
        [Required] [StringLength(250)] public string AA_Address { get; set; }
        [Required] public bool AA_Default { get; set; }


        public virtual Anag_Master Anag_Master { get; set; }
        public virtual Anag_Contact Anag_Contact { get; set; }
    }
}
