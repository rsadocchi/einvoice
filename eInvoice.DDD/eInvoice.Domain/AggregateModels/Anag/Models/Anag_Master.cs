﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "Anag_Master")]
    public class Anag_Master : Entity, IAggregateRoot
    {
        [Key] public int AM_ID { get; set; }
        [Required] public bool AM_NaturalPerson { get; set; }
        [Required] public DateTime AM_StartDate { get; set; }
        [StringLength(30)] public string AM_VATNo { get; set; }
        [StringLength(30)] public string AM_TaxCode { get; set; }
        [Required] [StringLength(150)] public string AM_LegalMail { get; set; }
        [Required] [StringLength(250)] public string AM_Name { get; set; }
        [StringLength(100)] public string AM_PersonFirstName { get; set; }
        [StringLength(100)] public string AM_PersonLastName { get; set; }
        [StringLength(30)] public string AM_Phone { get; set; }
        [StringLength(100)] public string AM_Email { get; set; }
        [StringLength(150)] public string AM_URI { get; set; }
        public int AM_Rating { get; set; }
        public string AM_Note { get; set; }



        public virtual Anag_Birth Anag_Birth { get; set; }

        public virtual ICollection<Anag_RoleXAnag> Anag_RoleXAnags { get; set; }
        public virtual ICollection<Anag_Address> Anag_Addresses { get; set; }
        public virtual ICollection<Anag_Contact> Anag_Contacts { get; set; }

        public virtual ICollection<Anag_Master> Customers { get; set; }
        public virtual ICollection<Anag_Master> Suppliers { get; set; }

        public Anag_Master()
        {
            Anag_RoleXAnags = new HashSet<Anag_RoleXAnag>();
            Anag_Addresses = new HashSet<Anag_Address>();
            Anag_Contacts = new HashSet<Anag_Contact>();

            Customers = new HashSet<Anag_Master>();
            Suppliers = new HashSet<Anag_Master>();
        }
    }
}
