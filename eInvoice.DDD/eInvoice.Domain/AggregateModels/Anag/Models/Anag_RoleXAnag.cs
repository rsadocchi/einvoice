﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "Anag_RoleXAnag")]
    public class Anag_RoleXAnag : Entity, IAggregateRoot
    {
        [Key] public int ARXA_ID { get; set; }
        public int? ARXA_AnagMasterID { get; set; }
        public int? ARXA_AnagContactID { get; set; }
        [Required] public int ARXA_RoleID { get; set; }



        public virtual EN_Roles EN_Roles { get; set; }
        public virtual Anag_Master Anag_Master { get; set; }
        public virtual Anag_Contact Anag_Contact { get; set; }
    }
}
