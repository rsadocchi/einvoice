﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "Anag_Birth")]
    public class Anag_Birth : Entity, IAggregateRoot
    {
        [Key] public int AB_ID { get; set; }
        public int? AB_AnagMasterID { get; set; }
        public int? AB_ContactID { get; set; }
        [Required] public DateTime AB_BirthDay { get; set; }
        [Required] [StringLength(5)] public string AB_Country { get; set; }
        [Required] [StringLength(5)] public string AB_CountrySpec { get; set; }
        [Required] [StringLength(10)] public string AB_PostCode { get; set; }
        [Required] [StringLength(50)] public string AB_City { get; set; }



        public virtual Anag_Master Anag_Master { get; set; }
        public virtual Anag_Contact Anag_Contact { get; set; }
    }
}
