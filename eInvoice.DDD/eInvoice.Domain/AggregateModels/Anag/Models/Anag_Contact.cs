﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    [Table(name: "Anag_Contact")]
    public class Anag_Contact : Entity, IAggregateRoot
    {
        [Key] public int AC_ID { get; set; }
        [Required] public int AC_AnagMasterID { get; set; }
        [Required] [StringLength(50)] public string AC_FirstName { get; set; }
        [Required] [StringLength(50)] public string AC_LastName { get; set; }
        [StringLength(30)] public string AC_Phone { get; set; }
        [StringLength(100)] public string AC_Email { get; set; }
        [StringLength(30)] public string AC_TaxCode { get; set; }
        [Required] public bool AA_Default { get; set; }


        public virtual Anag_Master Anag_Master { get; set; }
        public virtual Anag_Birth Anag_Birth { get; set; }
        public virtual Anag_Address Anag_Address { get; set; }
    }
}
