﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.Linq;

namespace eInvoice.Domain.AggregateModels
{
    public interface IDocumentRepository : IRepository<Doc_Master, int>
    {

    }
}
