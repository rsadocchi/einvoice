﻿using eInvoice.Domain.SeedWork;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace eInvoice.Domain.AggregateModels
{
    public class Doc_Master : Entity, IAggregateRoot
    {


        public virtual ICollection<Doc_Master> Doc_MasterInvoices { get; set; }
        public virtual ICollection<Doc_Master> Doc_MasterDDT { get; set; }

        public Doc_Master()
        {
            Doc_MasterInvoices = new HashSet<Doc_Master>();
            Doc_MasterDDT = new HashSet<Doc_Master>();
        }
    }
}
